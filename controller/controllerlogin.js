const express = require('express')
const bcrypt = require('bcrypt')
const otp = require('../otp_redis_mailer/otp')
const nodemailer = require('nodemailer');
const tables = require('../models/supplierCreationModel')
const findAllQueries = require('../models/findQueries')
const updateAllQueries = require('../models/updateQueries')


const supplier_model = require('../models/supplierLoginModel')
const client2 = require('../databases/database');
const client = (client2.pool._clients[0]);
const redisClient = otp.hello();
console.log(supplier_model.supplierLoginTable())
console.log(supplier_model.supplierLoginQuery())
const redis = require('redis');
var app = express();
require('dotenv').config();

//find
const findUniquecodeSupplierLogin = findAllQueries.find_unique_code_supplierlogin()
const findEmailSupplierLogin = findAllQueries.find_email_supplierlogin()
const findEmailPoorvikaLogin = findAllQueries.find_email_poorvikalogin()
const findEmailOtpsupplierlogin = findAllQueries.find_email_otp_supplierlogin()

//update

const updateEmailOtpsupplierlogin = updateAllQueries.update_otp_email_supplierlogin();
const updateEmailPasswordsupplierlogin = updateAllQueries.update_password_email_supplierlogin();









poorvikaRegisterNew = async (req, res) => {
    const tableSupllierLogin = (supplier_model.poorvikaLoginTable());
    const tableSupllierQuery = (supplier_model.poorvikaLoginQuery());

    console.log(tableSupllierLogin);
    client.query(tableSupllierLogin, (err, res) => {
        if (err) {
            console.log(err)
        }
    });
    const new_email = req.body.email;
    const new_value = await client.query(findEmailPoorvikaLogin, [new_email],)
    console.log(new_value)
    if (new_value.rowCount) {
        return res.status(409).send("email already exist");
    }
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const { name, email,otp } = req.body;
    if (!req.body.password || !name || !email) { return res.send("field should not be emty") };
    client.query(tableSupllierQuery, [name, email, hashedPassword,otp], (error, results) => {
        if (error) {
            throw error
        } else {
            res.send("you succesfully registered")
        }
    })
}



loginPoorvika = async (req, res, next) => {
    const email = (req.body.email)
    const password = (req.body.password)
    const new_value = await client.query(findEmailPoorvikaLogin, [email],)
    //console.log(new_value.rows[0].supplier_unique_number)
    if (!new_value.rowCount) {
        return res.status(401).send("email doesn't exist");
    }
    const valid_pass = await bcrypt.compare(password, new_value.rows[0].password)
    if (!valid_pass || email == '' || password == '') {
        return res.status(400).send("value shouldn't be null")
    }
    else if (valid_pass && email) {
        const user = new_value.rows[0].email;
        const salt = await bcrypt.genSalt(12)
        const hashedPassword = await bcrypt.hash(user, salt)
       // var aadhi = "login_token"
        console.log(new_value.rows[0].supplier_unique_number)

        redisClient.set(new_value.rows[0].supplier_unique_number, hashedPassword)
        console.log(new_value.rows[0].supplier_unique_number)
        console.log(hashedPassword)


        res.status(202).send({poorvika_unique_id :new_value.rows[0].supplier_unique_number,token : hashedPassword});
    }
}

registerNew = async (req, res) => {
    const tableSupllierLogin = (supplier_model.supplierLoginTable());
    const tableSupllierQuery = (supplier_model.supplierLoginQuery());

    console.log(tableSupllierLogin);
    client.query(tableSupllierLogin, (err, res) => {
        if (err) {
            console.log(err)
        }
    });
    const new_email = req.body.email;
    const new_value = await client.query(findEmailSupplierLogin, [new_email],)
    //.then((rew)=>{console.log(rew)},((error)=>{
      //  console.log(error)
    //}))
    console.log(new_value)
    if (new_value.rowCount) {
        return res.status(409).send("email already exist");
    }
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const { name, email,otp } = req.body;
    if (!req.body.password || !name || !email) { return res.send("field should not be emty") };
    client.query(tableSupllierQuery, [name, email, hashedPassword,otp], (error, results) => {
        if (error) {
            throw error
        } else {
            res.send("you succesfully registered")
        }
    })
}
loginSupplier = async (req, res, next) => {
    const email = (req.body.email)
    const password = (req.body.password)
    const new_value = await client.query(findEmailSupplierLogin, [email],)
    
    
    
    
    
    
    //console.log(new_value.rows[0].supplier_unique_number)
    if (!new_value.rowCount) {
        return res.status(401).send("email doesn't exist");
    }
    const valid_pass = await bcrypt.compare(password, new_value.rows[0].password)
    if (!valid_pass || email == '' || password == '') {
        return res.status(400).send("value shouldn't be null")
    }
    else if (valid_pass && email) {
        const user = new_value.rows[0].email;
        const salt = await bcrypt.genSalt(12)
        const hashedPassword = await bcrypt.hash(user, salt)
       // var aadhi = "login_token"
        console.log(new_value.rows[0].supplier_unique_number)

        redisClient.set(new_value.rows[0].supplier_unique_number, hashedPassword)
        console.log(new_value.rows[0].supplier_unique_number)
        console.log(hashedPassword)


        res.status(202).send({id :new_value.rows[0].supplier_unique_number,token : hashedPassword});
    }
}
forgotPassword = async (req, res) => {


    var email = req.params.email;
    const new_otp = otp.test();
    console.log(new_otp)
    const transporter = otp.mail_transporter();

    const new_value = await client.query(findEmailSupplierLogin, [email],)
    if (new_value.rowCount == 0) {
        return res.send("email does not exist")
    } else {

        client.query(updateEmailOtpsupplierlogin, [new_otp, email], (err, res) => {


            if (err) {
                return  err;
            }
        });
    }s

    var mailOptions = {
        from: 'noreply@poorvika.com ',
        to: email,
        subject: 'welcome to poorvika  portal',
        text: ` USE THIS ONE TIME PASSWORD(OTP) ${new_otp}  to RESET PASSWORD`,
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return res.send(error);
        } else {
            console.log(info.response);
        }
    })
    const salt = await bcrypt.genSalt(12)
    const hashedPassword = await bcrypt.hash(email, salt)
    redisClient.set(email, hashedPassword)
    res.status(201).send(hashedPassword);
}

resetPassword = async (req, res,err) => {


    const token = req.header('auth-token');

    if(!req.body.new_password || !req.body.otp_new) return res.status(400).send("password or otp should not be null");
    
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash[(req.body.new_password, salt)]
    var email = (req.params.email)
    var otp_new = (req.body.otp_new)
    var new_password = hashedPassword
    const new_value = await client.query(findEmailOtpsupplierlogin, [otp_new, email],)
    console.log(new_value.rowCount)
    if (new_value.rowCount == 0) {
        return res.status(404).send("otp incorrect")
    }
    client.query(updateEmailPasswordsupplierlogin, [new_password, email], (err, res) => {
        if (err) {
            res.send(err)
        }
    });
    redisClient.get(email, function (error1, reply) {

    if(token == reply){
   return res.status(201).send("your password updated");
    }  else{
       return res.send("token invalid")
    }

})
}

logout = async (req, res) => {
    var key = req.params.id 

    redisClient.DEL(key)

    res.status(200).send("logged off ")
}

deleteSupplierLogin = async (req, res) => {


    const supplier_number = req.params.id

    const new_value = await client.query(findUniquecodeSupplierLogin, [supplier_number],)

    if(new_value.rowCount ==0){

        return res.status(400).send("invalid supplier id")


    }

    else


    client.query("DELETE FROM supplierlogin WHERE Supplier_unique_Number = $1", [supplier_number], (err, result) => {

        if (err) {
            return res.send(err) ;
        }
        else {

            res.status(200).send("filed deleted")
        }
    })

}






module.exports = {

    
    registerNew,
    loginSupplier,
    forgotPassword,
    resetPassword,
    logout,
    deleteSupplierLogin,
    poorvikaRegisterNew,
    loginPoorvika
    






}


