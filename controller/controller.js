const express = require('express')
const bcrypt = require('bcrypt')
const otp = require('../otp_redis_mailer/otp')
const nodemailer = require('nodemailer');
const tables = require('../models/supplierCreationModel')
const supplier_model = require('../models/supplierLoginModel')
const findAllQueries = require('../models/findQueries')

const client2 = require('../databases/database');
const client = (client2.pool._clients[0]);
const redisClient = otp.hello();
console.log(supplier_model.supplierLoginTable())
console.log(supplier_model.supplierLoginQuery())
var cors = require('cors')
const findUniquecodeSupplierDetail = findAllQueries.find_unique_code_supplierdetail()
const findUniquecodeSupplierProduct = findAllQueries.find_unique_code_supplierproduct()



const redis = require('redis');
const { getOriginalNode } = require('typescript');
var app = express();
app.use(cors())

require('dotenv').config();

supplierDetail = async (req, res) => {
    const key = req.params.id
    const token = req.header('auth-token');
    redisClient.get(key, function (error1, reply) {
        if (error1) {
            console.log(error1)
            return res.send(err);
        }
        else if (!reply) {
            return res.send("invalid token")
        }
        else if (reply === token) {
            client.query('SELECT * FROM supplierdetail ', (error, results) => {
                if(error){

                    return res.send(error)
                } else
                return res.status(200).json(results.rows)
            })
        }
        else
            return res.status(401).send("mismatch")
    })
}
getSupplierLogin = async (req, res) => {
    supplier_number = req.params.id;
    const token = req.header('auth-token');
    redisClient.get(supplier_number, function (error1, reply) {
        if (error1) {
            console.log(error1)
            return res.send(err);
        }
        else if (!reply) {
            return res.send("invalid token")
        }
        else
            if (reply === token) {
                client.query(findUniquecodeSupplierDetail, [supplier_number], (error, results) => {
                    res.status(200).json(results.rows)
                })
            }
            else
                return res.status(401).send("token mismatch")

 })
}
postSupplier = async (req, res) => {
    const tableSupllierCreationTable = (tables.SupplierCreationTable());
    const supplierCreationQuery = tables.supplierCreationQuery();
    client.query(tableSupllierCreationTable, (err, res) => {
        if (err) {
            console.log(err)
        }
        console.log('Table is successfully created');
    });

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

   // const Supplier_unique_Number = req.params.Supplier_unique_Number;
    const { Supplier_code, Supplier_name, Communication_Address, Pincode, Contact_number, Mail_id,
        Pan_no, GST_IN, State, Due_Date, Bank_Name, IFSC_Code, Account_No, Branch_Name, City
    } = req.body;

    console.log(Mail_id)
    console.log(req.body);


    client.query('SELECT * FROM supplierdetail WHERE Mail_id =$1',[Mail_id], (error, results) => {
        if(results.rowCount){
        
             return res.send("email already registered")

        }
       
    console.log(results.rows)

    if(results.rows == 0){


    client.query(
        supplierCreationQuery,
        [Supplier_code, Supplier_name, Communication_Address, Pincode, Contact_number, Mail_id, Pan_no, GST_IN,
            State, Due_Date, Bank_Name, IFSC_Code, Account_No, Branch_Name, City,hashedPassword ], (error, results) => {
                if (error) {
                    return res.status(409).send(error)
                }
                 console.log(results);

                 client.query('SELECT * FROM supplierdetail WHERE Mail_id =$1',[Mail_id], (error, results) => {
                    
                           console.log(results.rows[0].supplier_unique_number)            
                    


                return res.status(201).send({ "supplier_unique_id" : results.rows[0].supplier_unique_number, "message": "supplier detail added" })
            })

            })

        }
    })

}
deleteSupplierCreation = async (req, res) => {

    const supplier_number = req.params.id

    const new_value = await client.query(findUniquecodeSupplierDetail, [supplier_number],)

    if (new_value.rowCount == 0) {

        return res.status(400).send("invalid supplier id")
    }

    else

        client.query("DELETE FROM supplierdetail WHERE Supplier_unique_Number = $1", [supplier_number], (err, result) => {

            if (err) {
                throw err;
            }
            else {

                res.status(200).send("delete")
            }
        })

}
updateSupplierDeatail = async (req, res) => {
    const supplierCreationUpdate = tables.supplierCreationUpdate();

    supplier_number = req.params.id;
    const new_value = await client.query(findUniquecodeSupplierDetail, [supplier_number],)
    if (new_value.rowCount == 0) {
return res.status(400).send("invalid supplier id")
    }
    else {
    const { Supplier_code, Supplier_name, Communication_Address, Pincode, Contact_number, Mail_id,
            Pan_no, GST_IN, State, Due_Date, Bank_Name, IFSC_Code, Account_No, Branch_Name, City
        } = req.body;
    client.query(supplierCreationUpdate,
            [Supplier_code, Supplier_name, Communication_Address, Pincode, Contact_number, Mail_id,
                Pan_no, GST_IN, State, Due_Date, Bank_Name, IFSC_Code, Account_No, Branch_Name, City, supplier_number], (err, res) => {

                if(err){
                    return res.send(err)
                } else{
            res.status(201).send("updated")
                }
    })
}
}

postSupplierProduct = async (req,res) => {

    const tablesupplierproductTable = (tables.SupplierProductTable());
    const supplierproductQuery = tables.supplierProductQuery();
    client.query(tablesupplierproductTable, (err, res) => {
        if (err) {
            console.log(err)
        }
        console.log('Table is successfully created');
    });

    var unique_id = req.params.id;


    client.query(findUniquecodeSupplierDetail,[unique_id], (error, results) => {
        if(!results.rowCount){
        
             return res.send("email doesn't exist")

        }

    })


    const {item_code,product_name,quantity,price,tax,storage,color} = req.body;

    client.query(
        supplierproductQuery,
        [item_code,product_name,quantity,price,tax,storage,color,unique_id ], (error, results) => {
                if (error) {
                    return res.status(409).send(error)
                } else {

                    return res.status(201).send({  "message": "supplier product added" })

                }

            })




}

getSupplierProduct = async(req,res) =>{
    var unique_id = req.params.id

    client.query(findUniquecodeSupplierProduct,[unique_id], (error, results) => {
        if(results.rowCount){
        
             return res.send(results.rows)

        }

    })

}

getAllSupplierProduct = async(req,res) =>{
    var unique_id = req.params.id

    client.query('SELECT * FROM supplierproduct', (error, results) => {
        if(results.rowCount){
        
             return res.send(results.rows)

        }

    })

}


deleteSupplierProduct = async (req, res) => {

    const supplier_number = req.params.id
    const item_number = req.params.item_number


    const new_value = await client.query(findUniquecodeSupplierProduct , [supplier_number],)

    if (new_value.rowCount == 0) {

        return res.status(400).send("invalid supplier id")
    }

    else

        client.query("DELETE FROM supplierproduct WHERE Supplier_unique_Number = $1 AND item_code = $2", [supplier_number,item_number], (err, result) => {

            if (err) {
                throw err;
            }
            else {

                res.status(200).send("delete")
            }
        })

}



module.exports = {

    supplierDetail,
    getSupplierLogin,
    postSupplier,
    updateSupplierDeatail,
    deleteSupplierCreation,
    postSupplierProduct,
    getSupplierProduct,
    getAllSupplierProduct,
    deleteSupplierProduct
}


