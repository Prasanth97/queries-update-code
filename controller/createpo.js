const express = require('express')
var axios = require('axios');
var FormData = require('form-data');
var data = new FormData();
const bcrypt = require('bcrypt')
const otp = require('../otp_redis_mailer/otp')
const nodemailer = require('nodemailer');
const tables = require('../models/modelCreatepo')
const client2 = require('../databases/database');
const client = (client2.pool._clients[0]);
const redisClient = otp.hello();
var cors = require('cors')
//const redis = require('redis');
var app = express();
app.use(cors())

require('dotenv').config();


createpoorder = async (req, res) => {
    const CreationPoTable = tables.CreationPoTable();
    const CreationPoQuery = tables.CreationPoQuery();
    client.query(CreationPoTable, (err, res) => {
        if (err) {
            console.log(err)
        }
        console.log('Table is successfully created');
    });



    // const Supplier_unique_Number = req.params.Supplier_unique_Number;
    const { suppliercode, suppliername, location, date, supplier_GSTIN, shipping_location_code,
        shipping_location_address, s_no, UOM, available_quantity, required_quantity, cost, tax_per, tax_amount, total_net_amount,
        company,email,address,city,contact_name,phone,address2,state
    } = req.body;








    client.query(
        CreationPoQuery,
        [suppliercode, suppliername, location, date, supplier_GSTIN, shipping_location_code,
            shipping_location_address, s_no, UOM, available_quantity, required_quantity, cost, tax_per, tax_amount, total_net_amount,
            company,email,address,city,contact_name,phone,address2,state], (error, results) => {
            if (error) {
                return res.status(409).send(error)
            }
            console.log(results);





            return res.status(201).send(" order successfully placed ")

        })



}

getSupplierCreatePo = async (req, res) => {

    var Supplier_unique_Number = req.params.id

    client.query('SELECT supplier_code,Supplier_name,Communication_Address,Due_Date,GST_IN  FROM supplierdetail WHERE Supplier_unique_Number =$1', [Supplier_unique_Number], (error, results) => {
        if (error) return res.status(404).send(error)

        //console.log(results.rows[0].supplier_unique_number)            



        return res.status(201).send(results.rows)
    })




}





getProductCreatePo = async (req, res) => {


    const value = "Nokia"

    var config = {
        method: 'get',
        url: `https://api.poorvikamobiles.net/api/apxapi/GetItemModelInfo?CompanyCode=PMPL&Product=0&Brand=${value}&ItemCategory=0&CreatedOnStartDate=0&CreatedOnEndDate=0&ModifiedOnStartDate=0&ModifiedOnEndDate=0&ItemNameLike=0&Status=0`,
        headers: {
            'userid': 'poorvika',
            'securitycode': '9528-6346-4652-2627',
            'Cookie': '__cfduid=d6ea7268344f5010c0e5e6d905fdfa1c71618038582',
            ...data.getHeaders()
        },
        data: data
    };



    axios(config)
        .then(function (response) {
            console.log(JSON.stringify(response.data));
            return res.send(response.data)
        })
        .catch(function (error) {
            return res.send(error);
        });



}




getbranchCreatePo = async (req, res) => {


var config = {
  method: 'get',
  url: 'https://api.poorvikamobiles.net/api/apxapi/GetBranchInfo?CompanyCode=PMPL&BranchCity=0&BranchPinCode=0&BranchState=0&StoreOpenStartDate=0&StoreOpenEndDate=0&ModifiedOnStartDate=0&ModifiedOnEndDate=0&Status=All',
  headers: { 
    'userid': 'poorvika', 
    'securitycode': '9528-6346-4652-2627', 
    'Cookie': '__cfduid=d6ea7268344f5010c0e5e6d905fdfa1c71618038582'
  }
};

axios(config)
.then(function (response) {
  console.log(JSON.stringify(response.data));
  return res.send(response.data)
})
.catch(function (error) {
  console.log(error);
  return res.send(error)

});


}






module.exports = {
    createpoorder,
    getSupplierCreatePo,
    getProductCreatePo,
    getbranchCreatePo,


}


