const express =  require('express');
const controller = require('../controller/controller')
var cors = require('cors')




const router = express.Router();
router.use(cors())


router.get('/supplier_detail/:id',controller.supplierDetail);
router.get('/getSupplier/:id/',controller.getSupplierLogin);
router.post('/postSupplier/',controller.postSupplier);
router.put("/updateSupplier/:id",controller.updateSupplierDeatail);    
router.delete("/creationDelete/:id",controller.deleteSupplierCreation);

router.post('/postSupplierProduct/:id',controller.postSupplierProduct);

router.get('/getSupplierProduct/:id/',controller.getSupplierProduct);
router.get('/getAllSupplierProduct/',controller.getAllSupplierProduct);
router.delete("/productDelete/:id/:item_number",controller.deleteSupplierProduct);











module.exports = router;






