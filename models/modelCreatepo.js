
module.exports= {


    CreationPoTable: () => {  return `
        CREATE table IF NOT EXISTS  createpo (
            suppliercode varchar,
            suppliername varchar,
            location varchar,
            date varchar,
            supplier_GSTIN varchar,
            shipping_location_code varchar,
            shipping_location_address varchar,
            s_no varchar,
            UOM varchar,
            available_quantity varchar,
            required_quantity int,
            cost int,
            tax_per int,
            tax_amount int,
            total_net_amount int,
            company varchar,
            email varchar,
            address varchar,
            city varchar,
            contact_name varchar,
            phone int,
            address2 varchar,
            state varchar
       );
        `;
    },



    CreationPoQuery: () => {  
        return  `INSERT INTO  createpo (suppliercode , suppliername, location,date,supplier_GSTIN,
            shipping_location_code,shipping_location_address,s_no,UOM,available_quantity,required_quantity,
            cost,tax_per,tax_amount,total_net_amount,company,email,address,city,contact_name,phone,address2,state)
            VALUES ($1, $2, $3, $4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23)`;
    
    },
    
    
    
    
    
    
    
    
    
    }


    //     Supplier_unique_Number int REFERENCES supplierdetail(Supplier_unique_Number) ON UPDATE CASCADE ON DELETE CASCADE PRIMARY KEY
