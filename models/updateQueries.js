update_otp_email_supplierlogin = () => {

    return "UPDATE supplierlogin SET otp = $1 WHERE email = $2"
}

update_password_email_supplierlogin = () => {

    return "UPDATE supplierlogin SET password = $1 WHERE email = $2"
}

module.exports = { update_otp_email_supplierlogin,update_password_email_supplierlogin}