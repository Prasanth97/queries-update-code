
module.exports= {


SupplierCreationTable: () => {  return `
    CREATE table IF NOT EXISTS  supplierdetail (
        Supplier_code varchar,
        Supplier_name varchar,
        Communication_Address varchar,
        Pincode int,
        Contact_number int,
        Mail_id varchar,
        Pan_no varchar,
        GST_IN varchar,
        State varchar,
        Due_Date varchar,
        Bank_Name varchar,
        IFSC_Code varchar,
        Account_No varchar,
        Branch_Name varchar,
        City varchar,
        Supplier_unique_Number int DEFAULT unique_rowid() PRIMARY KEY,
        password varchar
        



       );
    `;
},

supplierCreationQuery: () => {  
    return  `INSERT INTO  supplierdetail (Supplier_code , Supplier_name, Communication_Address,Pincode,Contact_number,
        Mail_id,Pan_no,GST_IN,State,Due_Date,Bank_Name,IFSC_Code,Account_No,Branch_Name,City,password) 
        VALUES ($1, $2, $3, $4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)`;

},


supplierCreationUpdate: () => {  
    return  "UPDATE supplierdetail SET  Supplier_code = $1 , Supplier_name =$2, Communication_Address =$3,Pincode =$4,Contact_number=$5,Mail_id =$6,Pan_no = $7,GST_IN = $8,State= $9,Due_Date = $10,Bank_Name =$11,IFSC_Code =$12,Account_No=$13,Branch_Name = $14,City =$15 WHERE Supplier_unique_Number = $16 ";

},


SupplierProductTable: () => {  return `
    CREATE table IF NOT EXISTS  supplierproduct (
        item_code varchar,
        product_name varchar,
        quantity varchar,
        price varchar,
        tax varchar,
        Supplier_unique_Number int,
        storage varchar ,
        color varchar


        
        



       );
    `;
},


supplierProductQuery: () => {  
    return  'INSERT INTO  supplierproduct (item_code , product_name,quantity, price,tax,storage,color,Supplier_unique_Number) VALUES ($1, $2, $3, $4,$5,$6,$7,$8)';

},








}



//     Supplier_unique_Number int REFERENCES supplierdetail(Supplier_unique_Number) ON UPDATE CASCADE ON DELETE CASCADE PRIMARY KEY
