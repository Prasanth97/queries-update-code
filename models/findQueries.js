
//table name supplierlogin with Supplier_unique_Number

find_unique_code_supplierlogin = () => {

    return 'SELECT * FROM supplierlogin WHERE Supplier_unique_Number =$1'
}


//table name supplierdetail with Supplier_unique_Number


find_unique_code_supplierdetail = () => {

    return 'SELECT * FROM supplierdetail WHERE Supplier_unique_Number =$1'
}

//table name supplierproduct with Supplier_unique_Number


find_unique_code_supplierproduct = () => {

    return 'SELECT * FROM supplierproduct WHERE Supplier_unique_Number =$1'
}


//table name supplierlogin with email
  
find_email_supplierlogin = () => {

    return 'SELECT * FROM supplierlogin WHERE email =$1'    //find_email_supplierlogin
}

//table name supplierlogin with email

find_email_poorvikalogin = () => {

    return 'SELECT * FROM poorvikalogin WHERE email =$1'
}


//table name supplierlogin with email

find_email_otp_supplierlogin = () => {

    return 'SELECT * FROM supplierlogin WHERE otp = $1 AND email = $2'
}


module.exports = { find_unique_code_supplierlogin, find_unique_code_supplierdetail,find_email_supplierlogin, find_unique_code_supplierproduct,find_email_poorvikalogin,find_email_otp_supplierlogin }

//find_email_supplierproduct,